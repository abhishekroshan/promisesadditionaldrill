const users = "https://jsonplaceholder.typicode.com/users";

// Function to fetch users from the API
function fetchUsers() {
  // Use 'fetch' to make a GET request to the Users API
  return fetch(users)
    .then((res) => {
      // Check if the response status is OK (200)
      if (!res.ok) {
        // If not OK, log an error message
        console.error("Some Error Occurred Fetching Data");

        throw new Error("Error Fetching Data");
      }
      // Parse the response as JSON and return the result
      return res.json();
    })
    .then((data) => {
      // Logging the user data received from the API
      console.log(data);
    })
    .catch((error) => {
      // Catch and log any errors that occurred during the process
      console.error(error);
    });
}

// Calling the function to start the data fetching
fetchUsers();
