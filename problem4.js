const users = "https://jsonplaceholder.typicode.com/users";

function problem4() {
  // Fetch the list of users
  return fetch(users)
    .then((res) => {
      // Check if the response status is OK (200)
      if (!res.ok) {
        console.log("Some Error Occured Fetching Users Data");

        throw new Error("Error Fetching Data");
      }
      // Parse the response body as JSON and return the user data
      return res.json();
    })
    .then((data) => {
      // Using Map over the list of users to create an array of promises
      const specificUserData = data.map((element) => {
        const id = element.id;

        // Fetch specific user data based on the user ID
        return fetch(
          `https://jsonplaceholder.typicode.com/users?id=${id}`
        ).then((res) => {
          // Check if the response status is OK (200) for every User
          if (!res.ok) {
            console.log(`Some error occured fetching user with id ${id}`);

            throw new Error("Error Fetching Data");
          }
          return res.json();
        });
      });
      //Using promise.all once all the data requests is completed
      return Promise.all(specificUserData);
    })
    .then((userData) => {
      userData.forEach((element) => {
        //Using for each to iterate in the Data array and logging each data
        console.log(element);
      });
    })
    .catch((error) => {
      // Handle any errors that occurred during the process
      console.error(error);
    });
}
/*---------------------------------------------------------------------------------------------- */

// Call the problem4 function to start the data fetching
problem4();
