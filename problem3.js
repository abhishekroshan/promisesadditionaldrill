// API URLs for users and todos
const users = "https://jsonplaceholder.typicode.com/users";
const todos = "https://jsonplaceholder.typicode.com/todos";

// Function to fetch users and todos
function fetchUsersAndTodos() {
  // Fetch users data
  return (
    fetch(users)
      .then((res) => {
        // Check if the response status is OK (200)
        if (!res.ok) {
          console.log("Some Error Occured Fetching Users Data");

          throw new Error("Error Fetching Data");
        }
        // Parse the response body as JSON and log the data
        return res.json();
      })
      .then((data) => {
        console.log(data);
      })
      // Chain another fetch for todos after users
      .then(() => {
        return fetch(todos);
      })
      .then((res) => {
        // Check if the response status is OK (200)
        if (!res.ok) {
          console.log("Some Error Occured Fetching Todos Data");

          throw new Error("Error Fetching Data");
        }
        // Parse the response body as JSON and log the data
        return res.json();
      })
      .then((data) => {
        console.log(data);
      })
      .catch((error) => {
        // Catch any errors that occurred during the process
        console.log(error);
      })
  );
}

// Calling the fetchUsersAndTodos function to start the data fetching
fetchUsersAndTodos();
