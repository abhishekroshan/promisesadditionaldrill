const todos = "https://jsonplaceholder.typicode.com/todos";

function problem5() {
  return fetch(todos)
    .then((res) => {
      // Check if the response status is OK (200)
      if (!res.ok) {
        console.log("Some Error occured fetching data");

        throw new Error("Error");
      }
      // Parse the response body as JSON and return the todos data
      return res.json();
    })
    .then((data) => {
      // Getting the userId from the first todo
      const firstTodo = data[0];

      const userId = firstTodo.userId;

      return fetch(
        `https://jsonplaceholder.typicode.com/todos?userId=${userId}`
      ).then((res) => {
        if (!res.ok) {
          console.log("Some error occured fetching data");

          throw new Error("Error");
        }
        return res.json();
      });
    })
    .then((firstTodo) => {
      // Log the todos data with userId 1
      firstTodo.forEach((element) => {
        console.log(element);
      });
    })
    .catch((error) => {
      console.log(error);
    });
}

// Call the problem5 function to initiate the data fetching

problem5();

/*------------------------------------------------------------------------------------------------------------------ */

// const firstTodo = "https://jsonplaceholder.typicode.com/todos?userId=1";

// function problem5() {
//   // Fetch the first todo
//   return fetch(firstTodo)
//     .then((res) => {
//       // Check if the response status is OK (200)
//       if (!res.ok) {
//         // Log an error message if there's an issue fetching the first todo
//         console.log("Some Error Occured");
//         // Throw an error to prevent subsequent solutions from executing
//         throw new Error("Error Fetching First Todo Data");
//       }
//       return res.json();
//     })
//     .then((data) => {
//       // Map over the first todo data to create an array of promises
//       const firstUserData = data.map((element) => {
//         const id = element.id;

//         // Fetch additional data for the user associated with the first todo
//         return fetch(
//           `https://jsonplaceholder.typicode.com/todos?id=${id}`
//         ).then((res) => {
//           // Check if the response status is OK (200)
//           if (!res.ok) {
//             console.log("Some Error Occured");

//             //throw an error to prevent subsequent solutions from executing
//             throw new Error("Error Fetching User Data");
//           }

//           return res.json();
//         });
//       });
//       // Wait for all user data requests to complete
//       return Promise.all(firstUserData);
//     })
//     .then((firstUserData) => {
//       // Log the user data associated with the first todo
//       firstUserData.forEach((element) => {
//         console.log(element);
//       });
//     })
//     .catch((error) => {
//       // Log any errors that occurred during the process
//       console.log(error);
//     });
// }

// problem5();
